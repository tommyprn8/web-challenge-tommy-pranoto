"use client";

import { useRouter } from "next/navigation";
import { useState } from "react";
import Image from "next/image";

import {
  BaseInput,
  BaseButton,
  BaseModal,
} from "../components";
import { userLogin } from "../utils";
import { logo, watermark } from "../assets";

export default function Home() {
  const [modalMessage, setModalMessage] =
    useState("");
  const [userInfo, setUserInfo] = useState({});
  const [error, setError] = useState(false);
  const router = useRouter();

  const onSubmit = () => {
    const username = document.getElementById(
      "username"
    ) as HTMLInputElement;
    const password = document.getElementById(
      "password"
    ) as HTMLInputElement;

    if (
      username.value !== "" &&
      password.value !== ""
    ) {
      userLogin({
        username: username.value,
        password: password.value,
      }).then((res) => {
        if (res !== "Invalid credentials") {
          setUserInfo(res);
          router.push("./products");
        } else {
          setError(true);
          setModalMessage(
            "User ID dan atau Password anda salah. Silahkan mencoba kembali"
          );
        }
      });
    } else {
      setError(true);
      setModalMessage(
        "User ID dan atau Password anda belum diisi. Silahkan isi terlebih dahulu"
      );
    }
  };

  const onCloseModal = () => {
    setError(false);
  };

  return (
    <main className="bg-broken-white h-screen w-screen flex flex-col items-center p-6">
      {error ? (
        <BaseModal
          onClick={onCloseModal}
          message={modalMessage}
        />
      ) : null}
      <div className="flex flex-col bg-white p-8 h-full w-[500px] justify-center relative">
        <Image
          alt="watermark"
          src={watermark}
          priority
          className="absolute top-0 left-0"
        />
        <div className="flex flex-col gap-6">
          <Image
            alt="logo"
            src={logo}
            className="self-center mb-10"
          />

          <div className="flex flex-col gap-2">
            <p className="font-bold text-2xl">
              Login
            </p>

            <p className="font-semibold">
              Please sign in to continue.
            </p>
          </div>

          <div className="flex flex-col gap-8">
            <BaseInput
              id="username"
              title="User ID"
              placeholder="User ID"
            />

            <BaseInput
              id="password"
              title="Password"
              placeholder="Password"
            />
          </div>

          <div className="flex justify-end">
            <BaseButton
              text="Login"
              onClick={onSubmit}
            />
          </div>
        </div>

        <p className="absolute bottom-5 left-1/4">
          Don't have an account?{" "}
          <span className="text-red-500 font-semibold cursor-pointer">
            Sign Up
          </span>
        </p>
      </div>
    </main>
  );
}
