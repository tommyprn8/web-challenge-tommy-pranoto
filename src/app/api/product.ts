import type {
  NextApiRequest,
  NextApiResponse,
} from "next";

type ProductsDataProps = {
  id: number;
  title: string;
  description: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
};

export interface ProductResponseProps {
  products: ProductsDataProps[];
  skip: number;
  total: number;
  limit: number;
}

export default function authHandler(
  req: NextApiRequest,
  res: NextApiResponse<ProductResponseProps>
) {
  if (req.method === "POST") {
    res.status(200).json({
      products: [
        {
          id: 1,
          title: "iPhone 9",
          description:
            "An apple mobile which is nothing like apple",
          price: 549,
          discountPercentage: 12.96,
          rating: 4.69,
          stock: 94,
          brand: "Apple",
          category: "smartphones",
          thumbnail: "...",
          images: ["...", "...", "..."],
        },
      ],

      total: 100,
      skip: 0,
      limit: 30,
    });
  }
}
