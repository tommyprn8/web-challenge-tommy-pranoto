import type {
  NextApiRequest,
  NextApiResponse,
} from "next";
type Data = {
  id: number;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  gender: string;
  image: string;
  token: string;
};

export default function authHandler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === "POST") {
    res.status(200).json({
      id: 15,
      username: "kminchelle",
      email: "kminchelle@qq.com",
      firstName: "Jeanne",
      lastName: "Halvorson",
      gender: "female",
      image:
        "https://robohash.org/autquiaut.png?size=50x50&set=set1",
      token: "eyJhbGciOiJ",
    });
  }
}
