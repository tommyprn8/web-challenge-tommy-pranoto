import BaseButton from "../base-button";

interface BaseModal {
  message: string;
  onClick: () => void;
}

export const BaseModal = ({
  message,
  onClick,
}: BaseModal) => {
  return (
    <div
      className={`absolute z-50 bg-black/50 h-screen w-screen top-0 left-0 right-0 flex justify-center items-center`}
    >
      <div className="bg-white rounded-xl h-[200px] w-[300px] flex flex-col p-4 justify-between ">
        <p className="self-center text-red-600 font-semibold">
          Oops!
        </p>
        <p className="text-sm">{message}</p>
        <BaseButton
          text="Tutup"
          onClick={onClick}
        />
      </div>
    </div>
  );
};

export default BaseModal;
