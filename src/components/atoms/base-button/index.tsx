interface BaseButtonProps {
  text: string;
  disabled?: boolean;
  classname?: string;
  onClick: () => void;
}

export const BaseButton = ({
  text,
  disabled,
  classname,
  onClick,
}: BaseButtonProps) => {
  return (
    <button
      disabled={disabled}
      className={`rounded-full border bg-indigo-500 text-white font-bold py-2 min-w-[120px] ${classname}`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default BaseButton;
