interface BaseInputProps {
  id: string;
  title: string;
  classname?: string;
  placeholder?: string;
}

export const BaseInput = ({
  id,
  title,
  classname,
  placeholder,
}: BaseInputProps) => {
  return (
    <div
      className={`flex flex-col gap-1 border-b border-black ${classname}`}
    >
      <p className="text-black">{title}</p>
      <input
        id={id}
        placeholder={placeholder}
        className="py-1 focus:!outline-none"
      />
    </div>
  );
};

export default BaseInput;
