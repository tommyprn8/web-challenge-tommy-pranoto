import BaseButton from "../../atoms/base-button";

interface ProductCard {
  name: string;
  image: string;
  detail: string;
  category: string;
  price: string;
  classname?: string;
}

export const ProductCard = ({
  name,
  image,
  detail,
  category,
  price,
  classname,
}: ProductCard) => {
  return (
    <div
      className={`bg-white border-2 border-black/30 rounded-md w-[200px] p-4 flex flex-col gap-4 ${classname}`}
    >
      <div>
        <div>
          <p className="font-semibold truncate">
            {name}
          </p>
          <p className="text-gray-400 text-sm">
            {category}
          </p>
        </div>
      </div>

      <img className="h-[100px]" src={image} />

      <p className="text-sm truncate">{detail}</p>

      <p className="text-sm ">Rp. {price}</p>

      <div className="self-end ">
        <BaseButton
          text={"Detail"}
          onClick={() => null}
          classname=""
        />
      </div>
    </div>
  );
};

export default ProductCard;
