"use client";
import { useState, useEffect } from "react";
import Image from "next/image";
import { getAllProducts } from "@/src/utils";
import ProductCard from "@/src/components/molecule/product-card";
import { ProductResponseProps } from "@/src/app/api/product";

import { logo } from "@/src/assets";

import "../../app/globals.css";

export default function ProductPage() {
  const [products, setProducts] = useState<
    ProductResponseProps["products"]
  >([]);

  useEffect(() => {
    getAllProducts().then((res) => {
      setProducts(res.products);
    });
  }, []);

  const productSection = (
    <div className="flex gap-2 overflow-y-hidden border-4 border-black/5 rounded-md p-4">
      {products?.map((item) => {
        return (
          <div key={item.id}>
            <ProductCard
              name={item.title}
              image={item.thumbnail}
              detail={item.description}
              category={item.category}
              price={item.price}
            />
          </div>
        );
      })}
    </div>
  );

  return (
    <div className="bg-broken-white min-h-screen max-w-screen flex flex-col justify-center items-center p-6">
      <Image
        alt="logo"
        src={logo}
        className="mb-8"
      />

      <div className="max-w-[90%]">
        <p className="font-semibold text-xl mb-2">
          Produk Pilihan Pengguna
        </p>
        {productSection}
      </div>

      <div className="max-w-[90%] mt-4">
        <p className="font-semibold text-xl mb-2">
          Produk Terlaris
        </p>
        {productSection}
      </div>
    </div>
  );
}
