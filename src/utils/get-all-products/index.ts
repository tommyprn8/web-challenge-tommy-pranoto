import axios from "axios";

export const getAllProducts = async () => {
  try {
    const response = await axios.get(
      `https://dummyjson.com/products`
    );

    if (response.status !== 200) {
      throw new Error(
        `Error! status: ${response.status}`
      );
    }

    return response.data;
  } catch (err: any) {
    return err.response.data.message;
  }
};

export default getAllProducts;
