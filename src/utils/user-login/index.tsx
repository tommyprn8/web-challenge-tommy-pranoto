import axios from "axios";

interface GetAuthInput {
  username: string;
  password: string;
}

export const userLogin = async ({
  username,
  password,
}: GetAuthInput) => {
  try {
    const response = await axios.post(
      `https://dummyjson.com/auth/login`,
      {
        username: username,
        password: password,
      }
    );

    if (response.status !== 200) {
      throw new Error(
        `Error! status: ${response.status}`
      );
    }

    return response.data;
  } catch (err: any) {
    return err.response.data.message;
  }
};

export default userLogin;
